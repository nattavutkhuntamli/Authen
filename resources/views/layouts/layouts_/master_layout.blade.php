<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบบทความ PHP @yield('title')</title>
    <link rel="shortcut icon" href="https://www.037hdmovie.com/wp-content/themes/037HDMovie.COM/fav.ico" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-color:#e9ecef;">

    {{-- header  --}}
    <nav class="navbar bg-light">
        <div class="container-fluid">
            <a href="#" class="nav-bar-brand">Navbar</a>
            <form action="{{route('logout')}}"method="POST">
                @csrf
                <button type="submit" class="btn btn-sm btn-primary">Logout</button>
            </form>
        </div>
    </nav>    

    {{-- Begin page content --}}
    @yield('content')
    {{-- footer --}}

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
