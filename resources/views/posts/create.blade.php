@extends('layouts.layouts_.master_layout')
@section('title')
    {{ $title }}
@endsection
@section('content')
    <main class="container mt-3" role="main">
        <div class="row mt-5">
            <div class="col-12 mx-auto">
                <div class="card rounded-bottom">
                    <div class="card-header bg-primary text-white">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Create Posts</h4>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('posts.index') }}" class="btn btn-success mx-1">Back</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row mb-3">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div class="alrt alert-danger">{{ $error }}</div>
                                @endforeach
                            @endif
                        </div>

                        <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-3">
                                <div class="form-group col-md-6">
                                    <div>
                                        <label for="" class="form-label">รูปภาพ</label>
                                        <input type="file" name="image" id="image" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div>
                                        <label for="" class="form-label">หัวข้อ</label>
                                        <input type="text" name="title" id="title" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div>
                                        <label for="" class="form-label">หมวดหมู่</label>
                                        <select name="category_id" id="category_id" class="form-control">
                                            <option value="#">เลือกรายการ</option>
                                            @isset($categories)
                                                @foreach ($categories as $categories)
                                                    <option value="{{ $categories->id }}">{{ $categories->name }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div>
                                        <label for="" class="form-label">รายละเอียด</label>
                                        <textarea class="form-control" placeholder="description" name="description" id="description" cols="30"
                                            rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary w-100 mt-3 rounded">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><br><br><br><br><br><br>
    </main>
@endsection
