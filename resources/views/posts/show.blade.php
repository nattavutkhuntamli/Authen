@extends('layouts.layouts_.master_layout')

@section('title') {{$title}} @endsection
@section('content')
<style>
    a{
    text-decoration: none;
    }
</style>
<main class="container mt-3" role="main">
    <div class="row mt-5">
        <div class="col-12 mx-auto">
            <div class="card rounded-bottom">
                <div class="card-header bg-primary text-white">
                    <div class="row">  
                        <div class="col-md-6">
                            <h4>Detail</h4>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <a href="{{route('posts.index')}}" class="btn btn-success mx-1">Back</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th width="25%">id</th>
                                <th scope="col">{{$posts->id}}</th>
                            </tr>
                            <tr>
                                <td width="30%">Image</td>
                               <td scope="col"><img src="{{asset($posts->images)}}" width="80"alt=""></td>
                            </tr>    
                            <tr>
                                <td width="20%">Description</td>
                                <td scope="col">{{Str::limit($posts->description,80)}}</td>
                            </tr>
                            <tr>
                                <td width="20%">Category</td>
                                <td scope="col"><div class="btn-sm btn-primary">{{$posts->category->name}}</div></td>
                            </tr>
                            <tr>
                                <td width="20%">Publish Date</td>
                                <td scope="col">{{date('d/m/Y',strtotime($posts->publish_date))}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><br><br><br><br><br><br>
</main>
@endsection