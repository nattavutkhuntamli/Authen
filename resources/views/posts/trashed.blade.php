
@extends('layouts.layouts_.master_layout')
@section('title')
    {{ $title }}
@endsection
@section('content')
    <main class="container mt-3" role="main">
        <div class="row mt-5">
            <div class="col-12 mx-auto">
                <div class="card rounded-bottom">
                    <div class="card-header bg-primary text-white">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Trashed Posts</h4>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('posts.index') }}" class="btn btn-success mx-1">Back</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead class="bg-primary text-white rounded">
                                <tr>
                                    <th scope="col"style="width:5%"class="text-center">#</th>
                                    <th scope="col"style="width:10%">Title</th>
                                    <th scope="col"style="width:30%">Description</th>
                                    <th scope="col"style="width:10%">Category</th>
                                    <th scope="col"style="width:10%">Publish Date</th>
                                    <th scope="col"style="width:15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $text = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe incidunt voluptatum voluptates est esse, asperiores ut minus rem fugit harum tempora fugiat in non mollitia ab, unde laboriosam ea consequatur.';
                                @endphp
                                @isset($posts)
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($posts as $posts)
                                        <tr>
                                            <th scope="col" class="text-center">{{ $i++ }}</th>
                                            <td scope="col"><img src="{{ asset($posts->images) }}"
                                                    width="80"alt=""></td>
                                            <td scope="col">{{ Str::limit($posts->description, 80) }}</td>
                                            <td scope="col">
                                                <div class="btn-sm btn-primary">{{ $posts->category->name }}</div>
                                            </td>
                                            <td scope="col">{{ date('d/m/Y', strtotime($posts->publish_date)) }}</td>
                                            <td scope="col">
                                                <a href="{{ route('posts.restore', $posts->id) }}"
                                                    class="btn-sm btn-success">Restore</a>
                                                <form action="{{ route('posts.force_delete', $posts->id) }}" method="post" class="mt-3">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn-sm btn-danger">Delete</button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><br><br><br><br><br><br>
    </main>
@endsection
