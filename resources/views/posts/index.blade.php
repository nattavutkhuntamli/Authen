@extends('layouts.layouts_.master_layout')
@section('title')
    {{ $title }}
@endsection
@section('content')
    <style>
        a {
            text-decoration: none;
        }
    </style>
    <main class="container mt-3" role="main">
        <div class="row mt-5">
            <div class="col-12 mx-auto">
                <div class="card rounded-bottom">
                    <div class="card-header bg-primary text-white">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>All Posts</h4>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('posts.create') }}" class="btn btn-success mx-1">Create</a>
                                <a href="{{ route('posts.tarshed') }}" class="btn btn-dark mx-1">Trashed</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead class="bg-primary text-white rounded">
                                <tr>
                                    <th scope="col"style="width:5%"class="text-center">#</th>
                                    <th scope="col"style="width:10%">Title</th>
                                    <th scope="col"style="width:30%">Description</th>
                                    <th scope="col"style="width:10%">Category</th>
                                    <th scope="col"style="width:10%">Publish Date</th>
                                    <th scope="col"style="width:35%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($posts)
                                    {{--  ตัวอย่างการแบ่ง pagetion แบบนับเลขกรณีไปหน้าที่2 ก็นับเลขต่อเลยไม่นับใหม่ --}}
                                    @php
                                        $rowNumber = ($posts->currentPage() - 1) * $posts->perPage();
                                    @endphp
                                    @
                                    @foreach ($posts as $key => $post)
                                        <tr>
                                            <th scope="col" class="text-center">{{ ++$rowNumber }}</th>
                                            <td scope="col"><img src="{{ asset($post->images) }}"
                                                    width="80"alt=""></td>
                                            <td scope="col">{{ Str::limit($post->description, 80) }}</td>
                                            <td scope="col">
                                                <div class="btn-sm btn-primary">{{ $post->category->name }}</div>
                                            </td>
                                            <td scope="col">{{ date('d/m/Y', strtotime($post->publish_date)) }}</td>
                                            <td scope="col">
                                                <a href="{{ route('posts.show', $post->id) }}"
                                                    class="btn-sm btn-success">Show</a>
                                                <a href="{{ route('posts.edit', $post->id) }}"
                                                    class="btn-sm btn-primary">Edit</a>
                                                <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn-sm btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                        {{-- ทำ pagetion  --}}
                        {{-- ให้ไปไฟล์ AppServiceProvider --}}
                        <div class="d-flex justify-content-end">
                            {!! $posts->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><br><br><br><br><br><br>
    </main>
@endsection
