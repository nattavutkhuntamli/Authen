<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File; //พวกเกียวกับ file

class PostController extends Controller
{

    public function __construct()
    {
        //$this->middleware('authCheck2')->only(['create','show']); // เรียกว่าการเช็ค middleware ผ่าน controller ส่วนในของ only คือไม่อนุญาติให้ function ต่อไปนี้เข้าได้
        //$this->middleware('authCheck2')->except(['index','show','create']); //อนุญาติให้ function ต่อไปนี้เข้าถึงได้
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = [
                'title' => 'การทำ CURD',
                'posts'  => Cache::remember('posts-page-'.request('page',1),3, function(){ //ดึงข้อมูล posts 
                    return Post::with('category')->paginate(10);
                }),
                // 'posts'  => Cache::rememberForever('posts', function(){
                //     return Post::with('category')->paginate(10);
                // })
            ];
            return view('posts.index', $data);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        try {
            $data = [
                'title' => 'การทำ create from',
                'categories' => Category::all()
            ];

            return view('posts.create', $data);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => ['required', 'image'],
            'title' => ['required', 'max:100'],
            'category_id' => ['required', 'integer'],
            'description' => ['required']
        ], [
            'image.required' => 'กรุณาเลือกไฟล์ที่จะอัพโหลดรูป',
            'image.image' => 'อัพได้เฉพาะไฟล์รูปเท่านั้น',

            'title.required' => 'กรุณาใส่ข้อมูลลงในช่องหัวข้อ',
            'title.max' => 'รับขนาดตัวอักษรสูงสุดไม่เกิน 100 ตัว',

            'category_id.required' => 'กรุณาเลือกหมวดรายการ',

            'description.required' => 'กรุณากรอกรายละเอียด'
        ]);
        $fileName = time() . '.' . $request->image->extension();
        $filePath = $request->image->storeAs('uploads', $fileName);

        $posts = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'images' => 'storage/' . $filePath,
            'publish_date' => date('Y-m-d'),
            'user_id' => rand(1, 4),
            'category_id' => $request->category_id,
            'views' => rand(1, 500),
            'status' => 1
        ]);

        if ($posts) {
            return redirect()->route('posts.index');
        } else {
            dd('flase');
        }

        //  $create = Post::insert([
        //     'title' => $request->title,
        //  ]);
        //  dd('success');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $data = [
                'title' => 'รายละเอียด',
                'posts' => Post::findOrFail($id)
            ];
            return view('posts.show', $data);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        try {
            $data = [
                'title' => 'edit from',
                'post'  => Post::findOrFail($id),
                'category' => Category::all()
            ];
            return view('posts.edit', $data);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => ['required', 'max:100'],
            'category_id' => ['required', 'integer'],
            'description' => ['required']
        ], [
            'title.required' => 'กรุณาใส่ข้อมูลลงในช่องหัวข้อ',
            'title.max' => 'รับขนาดตัวอักษรสูงสุดไม่เกิน 100 ตัว',

            'category_id.required' => 'กรุณาเลือกหมวดรายการ',

            'description.required' => 'กรุณากรอกรายละเอียด'
        ]);
        $post = Post::findOrFail($id);
        if ($request->hasFile('file')) {
            $request->validate([
                'file' => ['required', 'image']
            ], [
                'file.required' => 'กรุณาเลือกไฟล์ที่จะอัพโหลดรูป',
                'file.image' => 'อัพได้เฉพาะไฟล์รูปเท่านั้น',
            ]);
            $fileName = time() . '.' . $request->file->extension();
            $filePath = $request->file->storeAs('uploads', $fileName);
            File::delete(public_path($post->images));
            $post->images = 'storage/' . $filePath;
        }
        $post->title = $request->title;
        $post->description = $request->description;
        $post->publish_date  = date('Y-m-d');
        $post->user_id = rand(1, 4);
        $post->category_id = $request->category_id;
        $post->views = rand(1, 999);
        $post->status = rand(0, 1);
        $post->save();
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $post = Post::findOrFail($id);
        $post->delete($id);
        return redirect()->route('posts.index');
    }

    public function trashed()
    {
        try {
            $data = [
                'title' => 'การทำ trashed',
                'posts'  => Post::onlyTrashed()->get()
            ];
            return view('posts.trashed', $data);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restore(string $id){
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->restore();
        return redirect()->back();
    }
    public function forcedelete(string $id){
        $post = Post::withTrashed()->findOrFail($id);
        File::delete(public_path($post->images));
        $post->forceDelete();
        return redirect()->back();
    }
}
