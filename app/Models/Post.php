<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory ,SoftDeletes;
    protected $fillable = [
        'title',
        'description',
        'status',
        'publish_date',
        'images',
        'user_id',
        'category_id',
        'views',
    ];
    
    // protected $guarded = [
    //     'title',
    // ];


    /**
     * belongsTo ใน Laravel คือการกำหนดความสัมพันธ์ระหว่างโมเดลในลักษณะ "Belongs To" หรือ "เป็นส่วนหนึ่งของ"
     *ในระบบฐานข้อมูล ความสัมพันธ์นี้จะเชื่อมโยงโมเดลหนึ่งไปยังโมเดลอื่น แสดงถึงว่า โมเดลหนึ่งเป็นส่วนย่อยหรือมีความเกี่ยวข้องกับโมเดลอีกตัวหนึ่งในฐานข้อมูล
     *ใน Laravel, belongsTo ใช้สำหรับกำหนดความสัมพันธ์ระหว่างโมเดล โดยการระบุโมเดลที่เกี่ยวข้องกับโมเดลปัจจุบัน และสามารถใช้ได้ในทั้ง "One-to-One"
     *และ "One-to-Many" ความสัมพันธ์ โดยการใช้คีย์ต่างหากที่เรียกว่า "foreign key" เพื่อบ่งชี้ถึงแถวในตารางที่เกี่ยวข้อง
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * belongsToMany ใน Laravel คือการระบุความสัมพันธ์ของโมเดลระหว่างสองโมเดลแบบ "Many-to-Many" หรือ "มากต่อมาก"
     * ในระบบฐานข้อมูล โดยโมเดลสองตัวจะมีความสัมพันธ์กันหลายต่อหลาย คือ หนึ่งโมเดลสามารถมีความสัมพันธ์กับหลายโมเดลของอีกโมเดลหนึ่ง
     * และโมเดลดังกล่าวก็สามารถมีความสัมพันธ์กับโมเดลอื่นๆ ได้เช่นกันใน Laravel, เราใช้ belongsToMany เพื่อกำหนดความสัมพันธ์ระหว่างโมเดล
     * และใช้งานร่วมกับตารางกลางที่ใช้เพื่อเก็บข้อมูลเกี่ยวกับความสัมพันธ์นี้ โดยในตารางกลางนี้จะเก็บข้อมูลที่เชื่อมโยงระหว่างสองโมเดลรวมถึงข้อมูลเพิ่มเติม
     * ที่เกี่ยวข้องกับความสัมพันธ์นี้ด้วย เช่น ตารางกลางสามารถเก็บข้อมูลเพิ่มเติมเกี่ยวกับวันที่เริ่มและสิ้นสุดของความสัมพันธ์ระหว่างสองโมเดลได้
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
