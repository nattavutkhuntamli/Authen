<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
     /**
     * hasMany ใน Laravel คือการกำหนดความสัมพันธ์ระหว่างโมเดลในลักษณะ "Has Many" หรือ "มีหลาย" ในระบบฐานข้อมูล ความสัมพันธ์นี้แสดงถึงว่า
     * โมเดลหนึ่งมีความสัมพันธ์กับโมเดลอื่นหลายโมเดลในฐานข้อมูล โดยการใช้คีย์ต่างหากที่เรียกว่า "foreign key" เพื่อบ่งชี้ถึงแถวในตารางที่เกี่ยวข้อง
     * ใน Laravel, hasMany ใช้สำหรับกำหนดความสัมพันธ์ระหว่างโมเดล โดยการระบุโมเดลที่มีความสัมพันธ์ "มีหลาย" กับโมเดลปัจจุบัน
     * ความสัมพันธ์แบบนี้บ่งชี้ว่า โมเดลปัจจุบันเป็นเจ้าของข้อมูลในโมเดลที่เกี่ยวข้อง
     */

     public function posts()
     {
         //Post มีความสัมพันธ์ เป็นส่วนหนึ่งของ Category
         return $this->hasMany(Post::class);
     }
}
